# Holiday Calculator

(c) 2023 EICC, Inc.<br>
All Rigths Reserved

# Install

```bash
composer require eicc/holidaycalculator
```

# Use

```php
require_once 'vendor/autoload.php';

use Eicc\HolidayCalculator\HolidayCalculator;
use Eicc\HolidayCalculator\HolidayCollections\US;
use Eicc\HolidayCalculator\HolidayCollections\USFederal;
use Eicc\HolidayCalculator\HolidayCollections\OrthodoxChristian;

$hc = new HolidayCalculator(new USFederal());
$hc->addHolidays(new US());
$hc->addHolidays(new OrthodoxChristian());


echo "Holidays celebrated on 01/01/2023\n";
foreach ($hc->getHoliday(new DateTimeImmutable('01/01/2023')) as $key => $value) {
  echo $value['slug'] . "\n";
}

echo "\nWhen is Mother's Day 2030?\n";
echo $hc->get('mothers_day', 2030)[0]['date']->format('m/d/Y') . "\n";

echo "\nIs Today a holiday?\n";
echo $hc->isHoliday(new DateTimeImmutable()) ? "YES\n" : "NO\n";
echo "\nDone\n";
```

# Extend
If you want to add a personal collection of holidays, use the `data/` dir The namespace is:
```php
namespace Eicc\HolidayCalculator\ExternalCollections;

use Eicc\HolidayCalculator\HolidayCollections\AbstractHolidayCollection;

class MyExternalCollection extends AbstractHolidayCollection
{
  protected $holidayList = [
  ];
}
```

If you would like to add new Holiday collections to this repo, please do.

1. Fork this Repo
1. Add a new class in HolidayCollections, look at `US.php` as a template
1. Commit your changes to your repo
1. Submit a PR

Please submit each collection as a separate PR.

