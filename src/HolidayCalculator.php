<?php

namespace Eicc\HolidayCalculator;

use Eicc\HolidayCalculator\HolidayCollections\AbstractHolidayCollection;

/**
 * The core class of the holiday calculator system.
 *
 * It's basically a wrapper for the other Holiday classes.
 */
class HolidayCalculator
{
  protected array $holidays = [];

  public function __construct(AbstractHolidayCollection $holidays)
  {
    $this->addHolidays($holidays);
  }

  /**
   * Add new Holiday collections to the mix. You can pass 1 in on the
   * constructor but if you need more, you have to add them.
   */
  public function addHolidays(AbstractHolidayCollection $holidays): void
  {
    $className = get_class($holidays);
    if (!isset($this->holidays[$className])) {
      $this->holidays[get_class($holidays)] = $holidays;
    }
  }

  /**
   * Is the date passed in a holiday. This will return true if it is the date
   * of a holiday or the date of the observance of a holiday.
   *
   * If strict is set to true then it will ignore the observance date and only
   * calculate based on the date of the holiday.
   */
  public function isHoliday(\DateTimeImmutable $date, bool $strict = false): bool
  {
    return ! empty($this->getHoliday($date, $strict));
  }

  /**
   * get a list of slugs for the holidays for a given day. If none then it
   * returns an empty array.
   */
  public function getSlugs(\DateTimeImmutable $date, bool $strict = false): array
  {
    $returnValue = [];
    foreach ($this->getHoliday($date, $strict) as $value) {
      $returnValue[] = $value['slug'];
    }
    return $returnValue;
  }
  /**
   * Returns the full array of holidays celebrated on a given date. If strict
   * is passed in then it only looks at the date and not the date observed.
   */
  public function getHoliday(\DateTimeImmutable $date, bool $strict = false): array
  {
    $returnValue = [];
    foreach ($this->getObjectList() as $class => $thisHoliday) {
      $foundHoliday = $thisHoliday->getHoliday($date, $strict);

      if (!empty($foundHoliday)) {
        $returnValue[] = $foundHoliday;
      }
    }

    return $returnValue;
  }

  /**
   * Get the list of Holiday Collection Objects
   * [className] = object
   */
  protected function getObjectList(): array
  {
    foreach ($this->holidays as $class => $holiday) {
      $returnValue[$class] = $holiday;
    }
    return $returnValue;
  }

  /**
   * Get the complete list of holidays we know about.
   *
   * @todo be able to pass in a specific class name and limit the results to
   *       that.
   */
  public function getList(): array
  {
    $returnValue = [];
    foreach ($this->holidays as $class => $holidayList) {
      $returnValue[$class] = [];

      foreach ($holidayList->getList() as $key => $value) {
        $returnValue[$class][] = $value;
      }
    }

    return $returnValue;
  }

  protected function getMasterList(): array
  {
    return array_keys($this->holidays);
  }

  public function get(string $slug, int $year): array
  {
    $returnValue = [];
    foreach ($this->getObjectList() as $class => $thisHoliday) {
      $foundHoliday = $thisHoliday->get($slug, $year);

      if (!empty($foundHoliday) && !is_null($foundHoliday['date'])) {
        $returnValue[] = $foundHoliday;
      }
    }

    return $returnValue;
  }
}
