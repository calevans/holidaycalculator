<?php

namespace Eicc\HolidayCalculator\HolidayCollections;

use Eicc\HolidayCalculator\HolidayCollections\AbstractHolidayCollection;

class OrthodoxChristian extends AbstractHolidayCollection
{
  protected $holidayList = [
    "epiphany" => [
      'description' => 'Many people in the United States annually observe Epiphany, or Three Kings\u2019 Day, on January 6. It is a Christian observance and a public holiday in the US Virgin Islands.',
      'format' => 'january 6 %d',
      'weekendShift' => false
    ],
    "orthodox_christmas_day" => [
      'description' => 'Many Orthodox Christian churches in countries such as the United States observe Christmas Day on or near January 7 in the Gregorian calendar.',
      'format' => 'january 7 %d',
      'weekendShift' => false
    ],
  ];
}
