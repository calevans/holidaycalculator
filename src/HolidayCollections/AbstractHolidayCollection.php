<?php

namespace Eicc\HolidayCalculator\HolidayCollections;

/**
 *
 * Holiday list and rules come from https://www.law.cornell.edu/uscode/text/5/6103
 * Code started life at  https://stackoverflow.com/questions/14907561/how-to-get-date-for-holidays-using-php
 */

abstract class AbstractHolidayCollection
{
  /**
   * Pass in a holiday slug and a year, get back an array of the date of the
   * holiday and the date it is observed.
   */
  public function get(string $slug, int $year): array
  {
    $returnValue = [ 'date' => null, 'observed' => null ];
    if (! isset($this->holidayList[$slug])) {
      return $returnValue;
    }

    $holiday = $this->holidayList[$slug];
    $returnValue['date'] = new \DateTimeImmutable(sprintf($holiday['format'], $year));
    if ($holiday['weekendShift']) {
      $returnValue['observed'] = $this->observedDate(new \DateTimeImmutable(sprintf($holiday['format'], $year)));
    } else {
      $returnValue['observed'] = new \DateTimeImmutable(sprintf($holiday['format'], $year));
    }

    return $returnValue;
  }

  /**
   * Get a list of all holidays
   */
  public function getList(): array
  {
    $returnValue = [];
    foreach ($this->holidayList as $slug => $thisHoliday) {
      $returnValue[] = ['slug' => $slug, 'description' => $thisHoliday['description']];
    }
    return $returnValue;
  }

  /**
   * If the observance of the holiday is shifted because of the weekend.
   */
  protected function observedDate(\DateTimeImmutable $holiday)
  {
    $day = $holiday->format('w');

    if ($day == 6) {
      $returnValue = $holiday->sub(new \DateInterval('P1D'));
    } elseif ($day == 0) {
      $returnValue = $holiday->add(new \DateInterval('P1D'));
    } else {
      $returnValue = $holiday;
    }
    return $returnValue;
  }

  public function isHoliday(\DateTimeImmutable $date, bool $strict = false): bool
  {
    foreach ($this->getList() as $slug => $thisHoliday) {
      $holiday = $this->get($thisHoliday['slug'], $date->format('Y'));

      if ($holiday === []) {
        return false;
      }

      if ($date == $holiday['date'] || (!$strict && $date == $holiday['observed'])) {
        return true;
      }
    }
    return false;
  }

  public function getHoliday(\DateTimeImmutable $date, bool $strict = false): array
  {

    foreach ($this->getList() as $slug => $thisHoliday) {
      $holiday = $this->get($thisHoliday['slug'], $date->format('Y'));
      if ($holiday === []) {
        continue;
      }

      if (
          $date->format('Ymd') === $holiday['date']->format('Ymd')
          || (!$strict && $date->format('Ymd') === $holiday['observed']->format('Ymd'))
      ) {
        return ['slug' => $thisHoliday['slug'],'holiday' => $holiday];
      }
    }
    return [];
  }
}
