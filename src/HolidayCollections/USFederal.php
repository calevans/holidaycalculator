<?php

namespace Eicc\HolidayCalculator\HolidayCollections;

use Eicc\HolidayCalculator\HolidayCollections\AbstractHolidayCollection;

class USFederal extends AbstractHolidayCollection
{
  protected $holidayList = [
    "new_year" => [
      'description' => '',
      'format' => 'first day of january %d',
      'weekendShift' => true,
    ],
    "mlk_day" => [
      'description' => '',
      'format' => 'third monday january %d',
      'weekendShift' => false,
    ],
    "presidents_day" => [
      'description' => '',
      'format' => 'third monday february %d',
      'weekendShift' => false,
    ],
    "memorial_day" => [
      'description' => '',
      'format' => 'Last monday of May %d',
      'weekendShift' => true,
    ],
    "juneteenth" => [
      'description' => '',
      'format' => 'june 19 %d',
      'weekendShift' => true,
    ],
    "independence_day" => [
      'description' => '',
      'format' => 'july 4 %d',
      'weekendShift' => true,
    ],
    "labor_day" => [
      'description' => '',
      'format' => 'september %d first monday',
      'weekendShift' => false,
    ],
    "columbus_day" => [
      'description' => '',
      'format' => 'october %d second monday',
      'weekendShift' => false,
    ],
    "veterans_day" => [
      'description' => '',
      'format' => 'november 11 %d',
      'weekendShift' => true,
    ],
    "thanksgiving_day" => [
      'description' => '',
      'format' => 'fourth thursday november %d',
      'weekendShift' => false,
    ],
    "christmas_day" => [
      'description' => '',
      'format' => 'december 25 %d',
      'weekendShift' => true,
    ]
  ];
}
