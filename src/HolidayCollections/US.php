<?php

namespace Eicc\HolidayCalculator\HolidayCollections;

use Eicc\HolidayCalculator\HolidayCollections\AbstractHolidayCollection;

/**
 * Non-Federal US holidays and observances
 */
class US extends AbstractHolidayCollection
{
  protected $holidayList = [
    "mothers_day" => [
      'description' => "Mother's Day",
      'format' => 'second sunday of may %d',
      'weekendShift' => false
    ],
    "fathers_day" => [
      'description' => "Father's Day",
      'format' => 'third sunday of june %d',
      'weekendShift' => false
    ],
  ];
}
